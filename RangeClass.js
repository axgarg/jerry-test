'use strict';
// Task: Implement a class named 'RangeList'
// A pair of integers define a range, for example: [1, 5). This range includes integers: 1, 2, 3, and 4.
// A range list is an aggregate of these ranges: [1, 5), [10, 11), [100, 201)

/**
 * 
 * NOTE: Feel free to add any extra member variables/functions you like.
 */

class RangeList {
    /**
     * Adds a range to the list
     * @param {Array<number>} range - Array of two integers that specify beginning and end of range.
     */
    constructor() {
        this._globalList = [];
    }

    validate(range) {

        /* validations -
             - check that the range is an array
             - should be an array of integers
             - length of the input arra must be 2
             - lower limit and upper limit needs to ne honored
             - if the lower limit and upper limit numbers ae same, no action needs to be done
        */

        var isRangeAnArray = Array.isArray(range);
        if (!isRangeAnArray) 
            return new Error("input is not a valid data type. Array expected");
        
        var isLenValid = range.length == 2;
        if (!isLenValid) 
            return new Error("input is not of valid length Expected length 2");
        
        var isValidDataType = Number.isInteger(range[0]) && Number.isInteger(range[1]);
        if (!isValidDataType)
            return new Error("Expected integers as inputs");
        
        var isLimitHonored = range[0] < range[1]
        if(!isLimitHonored)
            return new Error("Expected lower limit should be lower than higher limit");

        return true;
    }

    add(range) {
             
        var rangeValidation = this.validate(range);

        if (rangeValidation.message) {
            return rangeValidation;
        }
        
        if (this._globalList.length == 0) {
            this._globalList.push(range);

        } else{

            var inputLowerLimit = range[0];
            var inputUpperLimit = range[1];
            var lengthOfGlobalList = this._globalList.length;

            for (var i = 0; i < lengthOfGlobalList; i++) {
                var val = this._globalList[i];
                var lowerLimit = val[0];
                var upperLimit = val[1];

                /*
                     we will always maintain a sorted list with the lower limit as the sorting key
                 */

                if (inputUpperLimit < lowerLimit) {
                    /*
                        case when the input value is less the the lower limit of the array value at the index.. In this case,
                        we would just insert the value at the specicied index.
                    */

                    this._globalList.splice(i,0, [inputLowerLimit, inputUpperLimit]);
                    break;

                } else if (inputLowerLimit > upperLimit) {
                    /*
                        case when the input value is more the the upper limit of the array value at the index.. 
                        In this case, we would just insert the value at the end of the array if it is the last index. Else, we shall
                        continue iterating over the nex index and check the cases.
                    */

                    if ( i == lengthOfGlobalList - 1 ) {
                        this._globalList.push([inputLowerLimit, inputUpperLimit]);
                        break;
                    } else {
                        continue;
                    }
                } 
                else if (inputUpperLimit <= upperLimit) {
                    /**
                     * if the upper limit of the input value falls within the bounds of the array value, then
                     * we need to modify the existing value with the lower limit being the Math.min(inputLowerLimit, lowerLimit)
                     */
                    
                    this._globalList[i][0] = Math.min(inputLowerLimit, lowerLimit);
                    break;
                } else if (inputUpperLimit > upperLimit) {
                    /**
                     * if the upper limit of the input value is greater than the max value of array at index, we need to modify 
                     * trhe successive elements as well if they fall within the bounds of the input max value.
                     */
                    var counter = i + 1;
                    var spliceArr = [];
                    var newUpperLimit = inputUpperLimit;
                    while(counter < lengthOfGlobalList) {
                        var newIndexLowerLimit = this._globalList[counter][0];
                        var newIndexUpperLimit = this._globalList[counter][1];

                        if(newUpperLimit < newIndexLowerLimit) {
                            break;
                        } else if (newUpperLimit <= newIndexUpperLimit) {
                            spliceArr.push(counter);
                            newUpperLimit = newIndexUpperLimit;
                            break;
                        } else if (newUpperLimit > newIndexUpperLimit) {
                            spliceArr.push(counter);
                            newUpperLimit = newIndexUpperLimit;
                        }
                    }
                    this._globalList[i][0] = Math.min(inputLowerLimit, lowerLimit);
                    this._globalList[i][1] = newUpperLimit;
                    spliceArr.forEach(index => {
                        this._globalList.splice(index, 1)
                    });
                    break;
                }
            }
        }
    }
  
    /**
     * Removes a range from the list
     * @param {Array<number>} range - Array of two integers that specify beginning and end of range.
     */
    remove(range) {
        
        var rangeValidation = this.validate(range);

        if(rangeValidation.message) {
            return rangeValidation;
        }

        var inputLowerLimit = range[0];
        var inputUpperLimit = range[1];
        var lengthOfGlobalList = this._globalList.length;

        for ( var i = 0; i < lengthOfGlobalList; i++ ) {
            var val = this._globalList[i];
            var lowerLimit = val[0];
            var upperLimit = val[1];

            if(inputUpperLimit <= lowerLimit || inputLowerLimit >= upperLimit) {
                /**
                 * if the input value is out of the bounds of array value, we may just iterate over the next elem.
                 */
                continue;

            } else if (inputLowerLimit <= lowerLimit && inputUpperLimit <= upperLimit) {
                /**
                 * if the upper limit falls within the bounds of array value, then we modify the array value.
                 */
                if (inputUpperLimit == upperLimit) {
                    this._globalList.splice(i, 1);
                } else {
                    this._globalList[i][0] = inputUpperLimit;
                }

                break;

            } else if (inputLowerLimit > lowerLimit && inputUpperLimit <= upperLimit) {

                /**
                 * if the range falls within the array value at the index. It would lead to possiblky two lists
                 */
                this._globalList[i][1] = inputLowerLimit;
                if(inputUpperLimit == upperLimit) {
                    break;
                }
                var insertIndex = i + 1;
                this._globalList.splice(insertIndex, 0, [inputUpperLimit, upperLimit]);
                break;

            } else if (inputLowerLimit <= lowerLimit && inputUpperLimit >= upperLimit) {
                /**
                 * if the array at the index value is within the range, remove the array index val and do the recursion 
                 * to take care of any subsequent elements that fall under the purview of range.
                 */
                this._globalList.splice(i, 1);
                return this.remove([upperLimit, inputUpperLimit]);

            } else if (inputLowerLimit > lowerLimit && inputUpperLimit > upperLimit) {
                /**
                 * if the input value min is between the array value and input max id outside the array values, 
                 * then just modify the record and do a recusion to check subseuent elements.
                 */
                this._globalList[i][1] = inputLowerLimit;
                return this.remove([upperLimit, inputUpperLimit]);                
            }

        }
    }
  
    /**
     * Prints out the list of ranges in the range list
     */
    print() {
        // TODO: implement this
        var response = [];
        this._globalList.forEach(val => {
            var item = "[" + val[0] + ", " + val[1] + ")";
            response = response ? response + " " + item : item;
        });
        console.log(response);
    }
  }

  module.exports = RangeList;



