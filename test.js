/* global describe it beforeEach afterEach */

'use strict';
const sinon = require('sinon');
const RangeList = require('./RangeClass');


describe('[Unit Test] Add element to list', function () {

    const rl = new RangeList();

    it('should add an element to the ranged list', done => {
        
        // Arrange
        const a = [1, 5];
        const b = [10, 20];
        const c = [10, 20];
        const d = [20, 20];
        const e = [20, 21];
        const f = [2, 4];
        const g = [3, 8];

        // Act
        rl.add(a);
        rl.add(b);
        rl.add(c);
        rl.add(d);
        rl.add(e);
        rl.add(f);
        rl.add(g);
        
        // Assert
        sinon.assert.match([1, 8], rl._globalList[0]);
        sinon.assert.match([10, 21], rl._globalList[1]);
        sinon.assert.match(2, rl._globalList.length);

        done();
    });

    it('should add a negative element to the ranged list', done => {
        
        // Arrange
        const a = [-5, -1];
        const b = [-20, -10];
        const c = [-20, -20];
        const d = [-21, -20];
        const e = [-4, -2];
        const f = [-8, -3];

        // Act
        rl.add(a);
        rl.add(b);
        rl.add(c);
        rl.add(d);
        rl.add(e);
        rl.add(f);
        
        // Assert
        sinon.assert.match([-21, -10], rl._globalList[0]);
        sinon.assert.match([-8, -1], rl._globalList[1]);
        // sinon.assert.match(4, rl._globalList.length);

        done();
    });

    it('should remove an element to the ranged list', done => {
        
        // Arrange
        const a = [10, 10];
        const b = [10, 11];
        const c = [15, 17];
        const d = [3, 19];
        const e = [-10, -10];
        const f = [-11, -10];
        const g = [-17, -15];
        const h = [-19, -3];

        // Act
        rl.remove(a);
        rl.remove(b);
        rl.remove(c);
        rl.remove(d);
        rl.remove(e);
        rl.remove(f);
        rl.remove(g);
        rl.remove(h);
        
        // Assert
        sinon.assert.match([-21, -19], rl._globalList[0]);
        sinon.assert.match([-3, -1], rl._globalList[1]);
        sinon.assert.match([1, 3], rl._globalList[2]);
        sinon.assert.match([19, 21], rl._globalList[3]);
        done();
    });
});




